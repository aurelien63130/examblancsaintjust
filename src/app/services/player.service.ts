import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Player} from "../models/player";

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Player[]> {
    return this.httpClient.get<Player[]>(this.apiUrl+'/players');
  }

  add(object: Player): Observable<Player> {
    return this.httpClient.post<Player>(this.apiUrl+'/players', object);
  }

  getOne(id: number): Observable<Player>{
    return this.httpClient.get<Player>(this.apiUrl+'/players/'+id);
  }

  remove(id: number): Observable<Player>{
    return this.httpClient.delete<Player>(this.apiUrl + '/players/'+id);
  }
}
