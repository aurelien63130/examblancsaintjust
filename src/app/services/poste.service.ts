import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PosteService {

  availablePosts = ["Guardien", "Défenseur", "Millieu", "Attaquant", "Remplacant"];

  constructor() { }
}
