import { Component, OnInit } from '@angular/core';
import {PosteService} from "../../services/poste.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  postes: string[] = this.posteService.availablePosts;

  constructor(private posteService: PosteService, private router: Router) { }

  ngOnInit(): void {
  }

  navigate(poste: string): void{
    this.router.navigate(["/"]).then(fn => {
      this.router.navigate(["poste", poste])
    })
  }

}
