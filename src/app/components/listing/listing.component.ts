import { Component, OnInit } from '@angular/core';
import {PlayerService} from "../../services/player.service";
import {Player} from "../../models/player";
import {PosteService} from "../../services/poste.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {

  players?: Player[];
  postes = this.posteService.availablePosts;

  constructor(private playerService: PlayerService,
  private posteService: PosteService, private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.playerService.getAll().subscribe(data => {
      this.players = data;
    })
  }

  deletePlayer(id: number) : void {
    this.playerService.remove(id).subscribe(data => {
      if(this.players){
        this.toastrService.info("Joueur supprimé !");
        this.players = this.players.filter(elem => elem.id != id);
      }
    });
  }

}
