import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoueurPosteComponent } from './joueur-poste.component';

describe('JoueurPosteComponent', () => {
  let component: JoueurPosteComponent;
  let fixture: ComponentFixture<JoueurPosteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoueurPosteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JoueurPosteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
