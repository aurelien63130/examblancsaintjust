import { Component, OnInit } from '@angular/core';
import {PlayerService} from "../../services/player.service";
import {Player} from "../../models/player";
import {ActivatedRoute} from "@angular/router";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-joueur-poste',
  templateUrl: './joueur-poste.component.html',
  styleUrls: ['./joueur-poste.component.css']
})
export class JoueurPosteComponent implements OnInit {
  players?: Player[];
  constructor(
    private playerService: PlayerService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService) { }

    ngOnInit(): void {
    let poste = this.activatedRoute.snapshot.paramMap.get('poste');
    this.playerService.getAll().subscribe(data => {
      this.players = data.filter(player => player.poste == poste);
    })
  }



  deletePlayer(id: number) : void {
    this.playerService.remove(id).subscribe(data => {
      if (this.players) {
        this.toastrService.info("Joueur supprimé !");
        this.players = this.players.filter(elem => elem.id != id);
      }
    });
  }
}
