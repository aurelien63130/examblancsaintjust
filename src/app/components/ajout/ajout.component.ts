import { Component, OnInit } from '@angular/core';
import {PosteService} from "../../services/poste.service";
import {Player} from "../../models/player";
import {Router} from "@angular/router";
import {PlayerService} from "../../services/player.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-ajout',
  templateUrl: './ajout.component.html',
  styleUrls: ['./ajout.component.css']
})
export class AjoutComponent implements OnInit {
  isLoading?: boolean;
  availablePost = this.posteService.availablePosts;
  formPlayer = new Player();

  constructor(private posteService: PosteService, private router: Router,
              private playerService: PlayerService, private toastrService: ToastrService) {
    this.formPlayer.poste = 'Guardien'
  }

  ngOnInit(): void {
    this.playerService.getAll().subscribe(data => {
      if(data.length > 22){
        this.toastrService.error("Accès INTERDIT !");
        this.router.navigate(["/"]);
      }
    })
  }

  addPlayer(): void{
    this.isLoading = true;
    this.playerService.add(this.formPlayer).subscribe(data => {
      this.isLoading = false;
      this.toastrService.success("Felicitations", data.prenom + ' séléctionné' );
      this.router.navigate(["/"]);
    })
  }

}
