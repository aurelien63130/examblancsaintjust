export class Player {
  id?: number;
  nom?: string;
  prenom?: string;
  poste?: string;
  image?: string;
}
