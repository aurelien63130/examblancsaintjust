import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListingComponent} from "./components/listing/listing.component";
import {AjoutComponent} from "./components/ajout/ajout.component";
import {DetailComponent} from "./components/detail/detail.component";
import {JoueurPosteComponent} from "./components/joueur-poste/joueur-poste.component";

const routes: Routes = [
  {path: '', component: ListingComponent},
  {path: 'nouveau', component: AjoutComponent},
  {path: ':id', component: DetailComponent},
  {path: 'poste/:poste', component: JoueurPosteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
